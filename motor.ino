void motorWrite(int pwm1, int pwm2)
{
  pwm1 = constrain(pwm1,-255,255);
  pwm2 = constrain(pwm2,-255,255);
  
  if(pwm1>0)
  {
    analogWrite(pinMotor1a, pwm1);
    analogWrite(pinMotor1b, 0);
  }
  else if(pwm1<0)
  {
    analogWrite(pinMotor1a, 0);
    analogWrite(pinMotor1b, -pwm1);
  }
  else if(pwm1==0)
  {
    analogWrite(pinMotor1a, 0);
    analogWrite(pinMotor1b, 0);
  }

  if(pwm2>0)
  {
    analogWrite(pinMotor2a, pwm2);
    analogWrite(pinMotor2b, 0);
  }
  else if(pwm2<0)
  {
    analogWrite(pinMotor2a, 0);
    analogWrite(pinMotor2b, -pwm2);
  }
  else if(pwm2==0)
  {
    analogWrite(pinMotor2a, 0);
    analogWrite(pinMotor2b, 0);
  }
}


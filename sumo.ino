
#define    DEBUG

//Aktuator
#define    pinMotor1a       6
#define    pinMotor1b       5
#define    pinMotor2a       10
#define    pinMotor2b       9

//Sensor
#define    pinFotodioda     0
#define    fotodiodaRead    analogRead(pinFotodioda)
#define    threshold        710
#define    adaGarisHitam    (fotodiodaRead>threshold)
#define    pinEcho          2
#define    pinTrig          3
uint16_t   distance;

//Strategi
bool          balik    = false;
unsigned long lastTime = 0;

void setup()
{
  pinMode(pinEcho,    INPUT);
  pinMode(pinTrig,    OUTPUT);
  Serial.begin(9600);
} 

void loop()
{
  
  if(!balik)
  {
    maju();
  }
  else
  {
    balikArah();
  }
}

void maju()
{
  #ifdef DEBUG
  Serial.print(fotodiodaRead);Serial.print(" ");
  Serial.println("Maju");
  #endif
  measureDistance();
  if((distance<15)&&(!adaGarisHitam))          //Ada robot lawan di depan
  {
    motorWrite(150,150);                       //Maju cepat
  }
  else if((distance>=15)&&(!adaGarisHitam))    //Tidak ada robot lawan 
  {
    motorWrite(120,120);                       //Maju pelan
  }
  else if(adaGarisHitam) 
  {
    motorWrite(0,0);
    lastTime = millis();
    balik = true;
  }    
}

void balikArah()
{
  #ifdef DEBUG
  Serial.print(fotodiodaRead);Serial.print(" ");
  Serial.println("Balik");
  #endif
  if(((millis()-lastTime)>=0)&&((millis()-lastTime)<350))
  motorWrite(-120,-120);                       //Mundur
  else if(((millis()-lastTime)>=350)&&((millis()-lastTime)<1000))
  motorWrite(120,-120);                        //Balik
  else if(millis()-lastTime>=1000)
  balik = false;
}




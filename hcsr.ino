
unsigned long pulse_length;

void measureDistance()
{
   digitalWrite(pinTrig, HIGH);
   delayMicroseconds(10);
   digitalWrite(pinTrig, LOW);
 
   pulse_length = pulseIn(pinEcho, HIGH);
   delay(50);
  
   distance = ( (unsigned int) (pulse_length / 58) );
}
